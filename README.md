# Ansible Role Sublime Text 3 [![Build Status](https://travis-ci.org/epok75/ansible-role-sublimetext-3.svg?branch=master)](https://travis-ci.org/epok75/ansible-role-sublimetext-3)

Role to manage Sublime Text 3.

## Requirements

This role requires Ansible 1.4 or higher, and platform requirements are listed in the metadata file.

## Role Variables

The variables that can be passed to this role and a brief description about them are as follows:

```yaml
# Default packages you want to install. Since there are no easy way to do it programatically, we just "git clone <repo>" inside "Packages" folder. (If some packages have others dependencies, it will probably not work or need extra work)
sublimetext_packages:
  - { url: "https://github.com/SideBarEnhancements-org/SideBarEnhancements.git", name: "SideBarEnhancements" }

# This is going to generate "Preferences.sublime-settings" file.
sublimetext_config:
  user:
    always_show_minimap_viewport: true
    bold_folder_labels: true
    draw_white_space: all
```

## Example Playbook

**requirements.yml**
```yaml
---
- src: git+https://gitlab.com/araulet-team/devops/ansible/ansible-role-sublimetext-3
  name: araulet.sublimetext-3
```

**playbook** file
```yaml
- hosts: all

- role: araulet.sublimetext3
    become: true
    sublimetext_packages:
      - { url: "https://github.com/SideBarEnhancements-org/SideBarEnhancements.git", name: "SideBarEnhancements" }
      - { url: "https://github.com/spadgos/sublime-jsdocs.git", name: "DocBlockr" }
      - { url: "https://github.com/sindresorhus/editorconfig-sublime.git", name: "EditorConfig" }
      - { url: "https://github.com/tmichel/sublime-terraform.git", name: "Terraform" }
      - { url: "https://github.com/jamiewilson/predawn", name: "Predawn" }
      - { url: "https://github.com/dracula/sublime.git", name: "Dracula Color Scheme" }
    sublimetext_config:
      user:
        file_exclude_patterns: []
        always_show_minimap_viewport: true
        bold_folder_labels: true
        draw_white_space: all
        theme_accent_purple: true,
        theme_bar: true,
        theme_bar_shadow_hidden: true,
        theme_dropdown_atomized: true,
        theme_find_panel_atomized: true,
        theme_sidebar_disclosure: true,
        theme_sidebar_folder_mono: true,
        theme_sidebar_indent_sm: true,
        theme_statusbar_colored: true,
        theme_statusbar_size_md: true,
        theme_tab_highlight_text_only: true,
        theme_tab_selected_filled: true,
        theme_tab_size_md: true,
        theme_tabset_line_visible: true,
        theme_unified: true
        # predawn setup
        theme: "predawn.sublime-theme"
        color_scheme: "Packages/Dracula Color Scheme/Dracula.tmTheme"
        predawn_findreplace_small: false
        predawn_quick_panel_small: false
        predawn_sidebar_arrows: false
        predawn_sidebar_folder_gray: false
        predawn_sidebar_large: false
        predawn_sidebar_medium: true
        predawn_sidebar_narrow: false
        predawn_sidebar_small: false
        predawn_sidebar_xlarge: false
        predawn_sidebar_xsmall: false
        predawn_tabs_active_underline: true
        predawn_tabs_large: false
        predawn_tabs_medium: false
        predawn_tabs_small: true
```

## Dependencies

None

## Licence

MIT

## Author Information

Arnaud Raulet